class ExchangeRate < Money::RatesStore::Memory
  def get_rate(from_iso_code, to_iso_code)
    transaction do
      Rails.cache.fetch(rate_key_for(from_iso_code, to_iso_code)) do
        if from_iso_code == base_currency
          Currency.find_by(code: to_iso_code).rate
        elsif to_iso_code == base_currency
          1 / Currency.find_by(code: from_iso_code).rate
        else
          Currency.find_by(code: to_iso_code).rate / Currency.find_by(code: from_iso_code).rate
        end
      end
    end
  end

  def rate_key_for(currency_iso_from, currency_iso_to)
    "RATE_FOR_#{super(currency_iso_from, currency_iso_to)}"
  end

  def base_currency
    'USD'
  end
end
