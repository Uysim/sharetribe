FROM ruby:2.3.4

MAINTAINER Sharetribe Team <team@sharetribe.com>

ENV REFRESHED_AT 2016-11-08

RUN apt-get update \
    && apt-get dist-upgrade -y

RUN curl -sL https://deb.nodesource.com/setup_7.x | bash - \
    && apt-get install -y nodejs


ENV SPHINX_VERSION 2.1.8



# Add helper for decrypting secure environment variables
RUN curl -sfSL \
  -o /usr/sbin/secure-environment \
  "https://github.com/convox/secure-environment/releases/download/v0.0.1/secure-environment" \
  && echo "4e4c1ed98f1ff4518c8448814c74d6d05ba873879e16817cd6a02ee5013334ea */usr/sbin/secure-environment" \
  | sha256sum -c - \
  && chmod 755 /usr/sbin/secure-environment

#
# Sharetribe
#

# Install nginx - used to serve maintenance mode page
RUN apt-get install -y nginx


RUN apt-get install -y mysql-client \
  cron

RUN wget -nv -O - http://sphinxsearch.com/files/sphinx-$SPHINX_VERSION-release.tar.gz | tar zx
RUN cd sphinx-$SPHINX_VERSION-release && \
  ./configure --with-mysql &&  \
  make && \
  make install clean

# remove sources
RUN rm -rf sphinx-$SPHINX_VERSION-release/


# Install latest bundler
ENV BUNDLE_BIN=
RUN gem install bundler

WORKDIR /opt/app

COPY Gemfile /opt/app
COPY Gemfile.lock /opt/app

ENV RAILS_ENV production

RUN bundle install --deployment --without test,development

COPY package.json /opt/app/

COPY client/package.json /opt/app/client/
COPY client/customloaders/customfileloader /opt/app/client/customloaders/customfileloader

ENV NODE_ENV production
ENV NPM_CONFIG_PRODUCTION true

RUN npm install --unsafe-perm

COPY . /opt/app
COPY config/database.docker.yml /opt/app/config/database.yml
COPY config/config.defaults.yml /opt/app/config/config.yml

EXPOSE 3000

#
# Assets
#




# If assets.tar.gz file exists in project root
# assets will be extracted from there.
# Otherwise, assets will be compiled with `rake assets:precompile`.
# Useful for caching assets between builds.
RUN script/prepare-assets.sh

VOLUME ["/opt/app/public/system", "/opt/app/db/sphinx", "/etc/letsencrypt", "/var/www/certbot"]

CMD ["script/startup.sh"]
ENTRYPOINT ["script/entrypoint.sh"]
