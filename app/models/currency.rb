# == Schema Information
#
# Table name: currencies
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  code       :string(255)
#  rate       :float(24)
#  symbol     :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Currency < ApplicationRecord
  has_many :people, :class_name => "People", :dependent => :destroy
  has_many :countries

  default_scope{ order(:code) }


  def self.supports
    codes = TransactionService::AvailableCurrencies::VALID_CURRENCIES.select { |key, value| value == :country_sets }.keys
    @stripe_supports = where(code: codes)
  end
end
