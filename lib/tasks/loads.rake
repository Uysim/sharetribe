namespace :loads do
  desc "Loads currencies countries and rates"
  task currencies: :environment do
    Rake::Task["imports:currencies"].invoke
    Rake::Task["imports:countries"].invoke
    Rake::Task["imports:currency_rates"].invoke
  end
end
