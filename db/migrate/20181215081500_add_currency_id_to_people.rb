class AddCurrencyIdToPeople < ActiveRecord::Migration[5.1]
  def change
    add_reference :people, :currency, index: true
  end
end
