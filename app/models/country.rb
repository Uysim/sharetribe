# == Schema Information
#
# Table name: countries
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  code        :string(255)
#  currency_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_countries_on_currency_id  (currency_id)
#

class Country < ApplicationRecord
  belongs_to :currency


  default_scope{ order(:name) }
end
