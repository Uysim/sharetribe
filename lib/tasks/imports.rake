namespace :imports do
  desc "Import Country with currencies"
  task currencies: :environment do
    file = File.read(Rails.root.join('lib', 'tasks', 'countries.json'))
    countries = JSON.parse(file)

    countries["results"].each do |key, value|
      existed_currency = Currency.find_by(code: value["currencyId"])
      if existed_currency.present?
        existed_currency.update_attributes({
          name: value["currencyName"],
          code: value["currencyId"],
          symbol: value["currencySymbol"]
        })
      else
        Currency.create({
          name: value["currencyName"],
          code: value["currencyId"],
          symbol: value["currencySymbol"]
        })
      end
    end
  end

  desc "Import Countries"
  task countries: :environment do
    file = File.read(Rails.root.join('lib', 'tasks', 'countries.json'))
    countries = JSON.parse(file)

    countries["results"].each do |key, value|
      existed_country = Country.find_by(code: value["id"])
      currency = Currency.find_by(code: value["currencyId"])
      if existed_country.present?
        existed_country.update_attributes({
          name: value["name"],
          code: value["id"],
          currency_id: currency.id
        })
      else
        Country.create({
          name: value["name"],
          code: value["id"],
          currency_id: currency.id
        })
      end
    end
  end

  desc "Import currency rate"
  task currency_rates: :environment do
    countries = Currency.all
    currency_rates_response = Net::HTTP.get(URI.parse("https://openexchangerates.org/api/latest.json?app_id=#{APP_CONFIG.openexchangerates_app_id}&base=USD"))
    currency_rates = JSON.parse(currency_rates_response)
    currency_rates["rates"].each do |key, value|
      currency = Currency.find_by(code: key)
      if currency.present?
        currency.update_attributes({rate: value})
      end
      puts "#{key} updated"
    end
  end

  desc "print currency rate"
  task print_rate: :environment do
    countries = Currency.all
    currency_rates_response = Net::HTTP.get(URI.parse("https://openexchangerates.org/api/latest.json?app_id=#{APP_CONFIG.openexchangerates_app_id}&base=USD"))
    currency_rates = JSON.parse(currency_rates_response)
    currency_rates["rates"].each do |key, value|
      puts "#{key}: #{value}"
    end
  end

end
